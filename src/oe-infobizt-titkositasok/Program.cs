﻿using System;

namespace oe_infobizt_titkositasok
{
    class Program
    {
        static void Main(string[] args)
        {
            //DiffieHellmann();
            CaesarEncryption();
            //ShannonTransformation();

            Console.ReadKey();
        }

        /// <summary>
        /// Diffie-Hellmann.
        /// </summary>
        private static void DiffieHellmann()
        {
            Console.WriteLine("Diffie-Hellmann kulcscsere...");

            var primeA = ReadNumberFromConsole("\"A\" prímszám: ");
            var primeB = ReadNumberFromConsole("\"B\" prímszám: ");

            var rndA = ReadNumberFromConsole("\"A\" véletlen: ");
            var rndB = ReadNumberFromConsole("\"B\" véletlen: ");

            var kOne = modK(primeB, rndA);
            var kTwo = modK(primeB, rndB);
            var kThree = modK(kTwo, rndA);
            var kFour = modK(kOne, rndB);

            Console.WriteLine($"K3: {kThree}\tK4: {kFour}");

            double modK(double num1, double num2) => Math.Pow(num1, num2) % primeA;
        }

        /// <summary>
        /// Executes the shannon transformation on a given text 
        /// based on the pre- and postpermutations, 
        /// the shift value and the number of the iterations.
        /// </summary>
        private static void ShannonTransformation()
        {
            Console.WriteLine("Shannon transzformáció");

            var msg = ReadStringFromConsoleWithMessage("Üzenet: ");
            var prePermutationStr = ReadStringFromConsoleWithMessage("Előpermutáció: ");
            var postPermutationStr = ReadStringFromConsoleWithMessage("Utópermutáció: ");
            var numOfIterations = ReadNumberFromConsole("Iterációk száma: ");
            var shift = ReadNumberFromConsole("Eltolás: ");
            var isEncrypting = ReadBooleanFromConsole("Encryption? (true | false): ");

            var shannon = new ShannonTransformationLogic(msg, prePermutationStr, postPermutationStr, shift, isEncrypting);

            for (int i = 0; i < numOfIterations; i++)
            {
                shannon.Iteration();

                Console.WriteLine($"Titkosított üzenet a(z) {i+1}. iterációnál:\t{shannon.Message}");
            }
        }

        private static void CaesarEncryption()
        {
            Console.WriteLine("Caesar...");

            var msg = ReadStringFromConsoleWithMessage("Üzenet: ");
            var shift = ReadNumberFromConsole("Eltolás: ");
            var isEncrypting = ReadBooleanFromConsole("Encryption? (true | false): ");

            var caesarText = CaesarEncryptor.GetCeasarEncryptedText(msg, isEncrypting ? shift : -shift);

            Console.WriteLine($"Titkosított üzenet: {caesarText}");
        }

        /// <summary>
        /// A helper method which helps reading a number from the console.
        /// </summary>
        /// <param name="msg">Message to display.</param>
        /// <returns>Number from the console.</returns>
        private static int ReadNumberFromConsole(string msg)
        {
            int num;
            string str;
            do
            {
                str = ReadStringFromConsoleWithMessage(msg);
            } while (!int.TryParse(str, out num));

            return num;
        }

        private static bool ReadBooleanFromConsole(string msg)
        {
            bool logic;
            string str;
            do
            {
                str = ReadStringFromConsoleWithMessage(msg);
            } while (!bool.TryParse(str, out logic));

            return logic;
        }

        /// <summary>
        /// Read a simple string with a message from the console.
        /// </summary>
        /// <param name="msg">Message to be displayed on the console.</param>
        /// <returns>String from console.</returns>
        private static string ReadStringFromConsoleWithMessage(string msg)
        {
            Console.Write(msg);
            return Console.ReadLine().ToUpper();
        }
    }
}
