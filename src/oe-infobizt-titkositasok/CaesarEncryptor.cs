﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oe_infobizt_titkositasok
{
    static class CaesarEncryptor
    {
        private const string ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private static char[] AbcArray => ABC.ToCharArray();

        /// <summary>
        /// Gets shifted ABC.
        /// </summary>
        /// <param name="shift">
        /// Shifting the ABC with this given value.
        /// e.g. the value is 5: F -> A
        /// </param>
        /// <returns></returns>
        private static char[] GetShiftedAbc(int shift)
        {
            // getting the abc as an array
            var abc = AbcArray;

            if (shift < 0)
            {
                shift = abc.Length + shift;
            }

            // this array represents the array which will be shifted
            char[] shiftedAbc = new char[ABC.Length];

            for (int idx = 0; idx < shiftedAbc.Length; idx++)
            {
                // shifting works like this:
                // if the "shift" value is 5
                //  then the sixth element will be the first one,
                //  and the seventh element will be the second one
                //  and so on...
                // e.g.: F => A, 
                //       G => B, etc...

                // so here the shiftedAbc's first element will be abc[shift]
                // and shift is increased by one at each iteration
                shiftedAbc[idx] = abc[shift++];

                // Once shift has reached abc.Length, it is resetted to 0
                // because values before abc[shift] weren't used.
                if(shift == abc.Length)
                {
                    shift = 0;
                }
            }

            // at this point the ABC is shifted and it can be returned
            return shiftedAbc;
        }

        /// <summary>
        /// Gets the Caesar encrypted text of a text.
        /// Basically in this method the ABC gets shifted
        /// and we will create a text based on the new, shifted ABC.
        /// </summary>
        /// <param name="text">Plain text.</param>
        /// <param name="shift">Shift value.</param>
        /// <returns>An encrypted text based on the new shifted ABC.</returns>
        public static string GetCeasarEncryptedText(string text, int shift)
        {
            // Getting the simple ABC as an array.
            var abc = AbcArray.ToList();

            // Getting the shifted ABC.
            var shiftedAbc = GetShiftedAbc(shift);

            // The encryped text has the same length as the original text.
            char[] encryptedText = new char[text.Length];

            for (int idx = 0; idx < text.Length; idx++)
            {
                // the element at idx in the encryptedText should be the following:
                // the new element which is represented in the shiftedAbc
                // for example the text's element at idx is: F
                //  in this case we want to know the index of 'F' in the ABC
                //  once it is found, we will look what is the new character
                //  at idx in the shiftedAbc.
                //  if the abc is shifted by 5, this will be 'K'
                encryptedText[idx] = shiftedAbc[abc.IndexOf(text[idx])];
            }

            // The 'encrypted' text is returned as a string instead of a char array.
            return String.Join("", encryptedText);
        }
    }
}
