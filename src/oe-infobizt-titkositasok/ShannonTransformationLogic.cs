﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace oe_infobizt_titkositasok
{
    class ShannonTransformationLogic
    {
        public string Message { get; private set; }
        private List<int> prePermutation;
        private List<int> postPermutation;
        private int shift;
        private int Shift => this.isEncrypting ? shift : -shift;
        private bool isEncrypting;

        public ShannonTransformationLogic(string message, string prePermutation, string postPermutation, int shift, bool isEncrypting)
        {
            this.Message = message;
            this.prePermutation = GetNumArrayFromNumberString(prePermutation);
            this.postPermutation = GetNumArrayFromNumberString(postPermutation);
            this.shift = shift;
            this.isEncrypting = isEncrypting;

            List<int> GetNumArrayFromNumberString(string numStr) => numStr.ToCharArray().Select(c => int.Parse(c.ToString())).ToList();
        }

        public void Iteration()
        {
            if(isEncrypting)
            {
                Message = PrePermutation();
                Message = PostPermutation();
            }
            else
            {
                Message = PostPermutation();
                Message = PrePermutation();
            }
        }

        private string PrePermutation() => GetPermutatedStr(prePermutation, Message);

        private string PostPermutation() => GetPermutatedStr(postPermutation, CaesarEncryptor.GetCeasarEncryptedText(Message, Shift));

        private string GetPermutatedStr(List<int> permutation, string message)
        {
            var text = new char[message.Length];
            for (int idx = 0; idx < message.Length; idx++)
            {
                if(isEncrypting)
                {
                    text[permutation[idx] - 1] = message[idx];
                }
                else
                {
                    text[permutation.IndexOf(idx + 1)] = message[idx];
                }
            }
            return String.Join("", text);
        }
    }
}
